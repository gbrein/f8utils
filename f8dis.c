/*
 * Copyright (c) 2022 Georg Brein. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */


#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <string.h>
#include <ctype.h>
#include <errno.h>
#include <stdarg.h>
#include <assert.h>


/*
 * formats used to format hexadecimal constants  in the disassembler output
 */
#define FMT_DIGIT "$%1x"
#define FMT_BYTE "$%02x"
#define FMT_WORD "$%04x"


/*
 * program name for stderr messages
 */
static const char *prog_name = NULL;


/*
 * possible program section types
 */
enum section {
	SEC_CODE,
	SEC_BYTES,
	SEC_ASCII,
	SEC_WORDS,
	SEC_LABELS,
	SEC_UNUSED
};


/*
 * program sections as text strings
 */
static const char *section_names[] = {
	"code",
	"bytes",
	"ascii",
	"words",
	"labels",
	"unused",
	NULL
};


/*
 * data structure describing a program section
 */
struct control {
	int addr;
	enum section section;
	struct control *next_p;
};


/*
 * data structure describing a label
 */
struct label {
	int addr;
	char *text;
	struct label *next_p;
};


/*
 * address of the first byte of the code
 */
static int origin = 0;
/*
 * list of sections in the code (sorted by addr)
 */
static struct control *control_p = NULL;
/*
 * list of labels in the code (sorted by addr)
 */
static struct label *label_p = NULL;
/*
 * code and its length
 */
static unsigned char *image = NULL;
static size_t l_image = 0;
/*
 * configuration file name and line for error messages
 */
static const char *config_fn;
static int config_ln = 0;


/*
 * types of instructions
 */
enum op_type {
	OP_NONE,
	OP_FIXED,
	OP_IMM8,
	OP_IMM16,
	OP_ILL,
	OP_SCR,
	OP_ASCR,
	OP_SCRA,
	OP_REL,
	OP_BTREL,
	OP_BFREL,
	OP_IMM3,
	OP_IMM4
};


/*
 * number of bytes per instruction type
 */
static const int op_length[] = { 1, 1, 2, 3, 1, 1, 1, 1, 2, 2, 2, 1, 1 };


/*
 * structure describing a single opcode
 */
struct opcode {
	enum op_type op_type;
	const char *mnemonic;
	const char *fixed_param;
};
	
	
/*
 * mnemonics used by more than one opcode
 */
static const char mn_as[]  = "as";
static const char mn_asd[]  = "asd";
static const char mn_bf[]  = "bf";
static const char mn_bt[]  = "bt";
static const char mn_ds[]  = "ds";
static const char mn_ins[]  = "ins";
static const char mn_lis[]  = "lis";
static const char mn_lisl[]  = "lisl";
static const char mn_lisu[]  = "lisu";
static const char mn_lr[]  = "lr";
static const char mn_ns[]  = "ns";
static const char mn_outs[]  = "outs";
static const char mn_sl[]  = "sl";
static const char mn_sr[]  = "sr";
static const char mn_xs[]  = "xs";
static const char mn_db[]  = "dc.b";
static const char mn_equ[]  = "equ";


/*
 * table of opcodes
 */
static struct opcode opcodes[256] = {
/* 00 */	{ OP_FIXED,	mn_lr,		"a,ku" },
/* 01 */	{ OP_FIXED,	mn_lr,		"a,kl" },
/* 02 */	{ OP_FIXED,	mn_lr,		"a,qu" },
/* 03 */	{ OP_FIXED,	mn_lr,		"a,ql" },
/* 04 */	{ OP_FIXED,	mn_lr,		"ku,a" },
/* 05 */	{ OP_FIXED,	mn_lr,		"kl,a" },
/* 06 */	{ OP_FIXED,	mn_lr,		"qu,a" },
/* 07 */	{ OP_FIXED,	mn_lr,		"ql,a" },
/* 08 */	{ OP_FIXED,	mn_lr,		"k,p" },
/* 09 */	{ OP_FIXED,	mn_lr,		"p,k" },
/* 0a */	{ OP_FIXED,	mn_lr,		"a,is" },
/* 0b */	{ OP_FIXED,	mn_lr,		"is,a" },
/* 0c */	{ OP_NONE,	"pk",		NULL },
/* 0d */	{ OP_FIXED,	mn_lr,		"p0,q" },
/* 0e */	{ OP_FIXED,	mn_lr,		"q,dc" },
/* 0f */	{ OP_FIXED,	mn_lr,		"dc,q" },
/* 10 */	{ OP_FIXED,	mn_lr,		"dc,h" },
/* 11 */	{ OP_FIXED,	mn_lr,		"h,dc" },
/* 12 */	{ OP_FIXED,	mn_sr,		"1" },
/* 13 */	{ OP_FIXED,	mn_sl,		"1" },
/* 14 */	{ OP_FIXED,	mn_sr,		"4" },
/* 15 */	{ OP_FIXED,	mn_sl,		"4" },
/* 16 */	{ OP_NONE,	"lm",		NULL },
/* 17 */	{ OP_NONE,	"st",		NULL },
/* 18 */	{ OP_NONE,	"com",		NULL },
/* 19 */	{ OP_NONE,	"lnk",		NULL },
/* 1a */	{ OP_NONE,	"di",		NULL },
/* 1b */	{ OP_NONE,	"ei",		NULL },
/* 1c */	{ OP_NONE,	"pop",		NULL },
/* 1d */	{ OP_FIXED,	mn_lr,		"w,j" },
/* 1e */	{ OP_FIXED,	mn_lr,		"j,w" },
/* 1f */	{ OP_NONE,	"inc",		NULL },
/* 20 */	{ OP_IMM8,	"li",		NULL },
/* 21 */	{ OP_IMM8,	"ni",		NULL },
/* 22 */	{ OP_IMM8,	"oi",		NULL },
/* 23 */	{ OP_IMM8,	"xi",		NULL },
/* 24 */	{ OP_IMM8,	"ai",		NULL },
/* 25 */	{ OP_IMM8,	"ci",		NULL },
/* 26 */	{ OP_IMM8,	"in",		NULL },
/* 27 */	{ OP_IMM8,	"out",		NULL },
/* 28 */	{ OP_IMM16,	"pi",		NULL },
/* 29 */	{ OP_IMM16,	"jmp",		NULL },
/* 2a */	{ OP_IMM16,	"dci",		NULL },
/* 2b */	{ OP_NONE,	"nop",		NULL },
/* 2c */	{ OP_NONE,	"xdc",		NULL },
/* 2d */	{ OP_ILL,	NULL,		NULL },
/* 2e */	{ OP_ILL,	NULL,		NULL },
/* 2f */	{ OP_ILL,	NULL,		NULL },
/* 30 */	{ OP_SCR,	mn_ds,		NULL },
/* 31 */	{ OP_SCR,	mn_ds,		NULL },
/* 32 */	{ OP_SCR,	mn_ds,		NULL },
/* 33 */	{ OP_SCR,	mn_ds,		NULL },
/* 34 */	{ OP_SCR,	mn_ds,		NULL },
/* 35 */	{ OP_SCR,	mn_ds,		NULL },
/* 36 */	{ OP_SCR,	mn_ds,		NULL },
/* 37 */	{ OP_SCR,	mn_ds,		NULL },
/* 38 */	{ OP_SCR,	mn_ds,		NULL },
/* 39 */	{ OP_SCR,	mn_ds,		NULL },
/* 3a */	{ OP_SCR,	mn_ds,		NULL },
/* 3b */	{ OP_SCR,	mn_ds,		NULL },
/* 3c */	{ OP_SCR,	mn_ds,		NULL },
/* 3d */	{ OP_SCR,	mn_ds,		NULL },
/* 3e */	{ OP_SCR,	mn_ds,		NULL },
/* 3f */	{ OP_ILL,	NULL,		NULL },
/* 40 */	{ OP_ASCR,	mn_lr,		NULL },
/* 41 */	{ OP_ASCR,	mn_lr,		NULL },
/* 42 */	{ OP_ASCR,	mn_lr,		NULL },
/* 43 */	{ OP_ASCR,	mn_lr,		NULL },
/* 44 */	{ OP_ASCR,	mn_lr,		NULL },
/* 45 */	{ OP_ASCR,	mn_lr,		NULL },
/* 46 */	{ OP_ASCR,	mn_lr,		NULL },
/* 47 */	{ OP_ASCR,	mn_lr,		NULL },
/* 48 */	{ OP_ASCR,	mn_lr,		NULL },
/* 49 */	{ OP_ASCR,	mn_lr,		NULL },
/* 4a */	{ OP_ASCR,	mn_lr,		NULL },
/* 4b */	{ OP_ASCR,	mn_lr,		NULL },
/* 4c */	{ OP_ASCR,	mn_lr,		NULL },
/* 4d */	{ OP_ASCR,	mn_lr,		NULL },
/* 4e */	{ OP_ASCR,	mn_lr,		NULL },
/* 4f */	{ OP_ILL,	NULL,		NULL },
/* 50 */	{ OP_SCRA,	mn_lr,		NULL },
/* 51 */	{ OP_SCRA,	mn_lr,		NULL },
/* 52 */	{ OP_SCRA,	mn_lr,		NULL },
/* 53 */	{ OP_SCRA,	mn_lr,		NULL },
/* 54 */	{ OP_SCRA,	mn_lr,		NULL },
/* 55 */	{ OP_SCRA,	mn_lr,		NULL },
/* 56 */	{ OP_SCRA,	mn_lr,		NULL },
/* 57 */	{ OP_SCRA,	mn_lr,		NULL },
/* 58 */	{ OP_SCRA,	mn_lr,		NULL },
/* 59 */	{ OP_SCRA,	mn_lr,		NULL },
/* 5a */	{ OP_SCRA,	mn_lr,		NULL },
/* 5b */	{ OP_SCRA,	mn_lr,		NULL },
/* 5c */	{ OP_SCRA,	mn_lr,		NULL },
/* 5d */	{ OP_SCRA,	mn_lr,		NULL },
/* 5e */	{ OP_SCRA,	mn_lr,		NULL },
/* 5f */	{ OP_ILL,	NULL,		NULL },
/* 60 */	{ OP_IMM3,	mn_lisu,	NULL },
/* 61 */	{ OP_IMM3,	mn_lisu,	NULL },
/* 62 */	{ OP_IMM3,	mn_lisu,	NULL },
/* 63 */	{ OP_IMM3,	mn_lisu,	NULL },
/* 64 */	{ OP_IMM3,	mn_lisu,	NULL },
/* 65 */	{ OP_IMM3,	mn_lisu,	NULL },
/* 66 */	{ OP_IMM3,	mn_lisu,	NULL },
/* 67 */	{ OP_IMM3,	mn_lisu,	NULL },
/* 68 */	{ OP_IMM3,	mn_lisl,	NULL },
/* 69 */	{ OP_IMM3,	mn_lisl,	NULL },
/* 6a */	{ OP_IMM3,	mn_lisl,	NULL },
/* 6b */	{ OP_IMM3,	mn_lisl,	NULL },
/* 6c */	{ OP_IMM3,	mn_lisl,	NULL },
/* 6d */	{ OP_IMM3,	mn_lisl,	NULL },
/* 6e */	{ OP_IMM3,	mn_lisl,	NULL },
/* 6f */	{ OP_IMM3,	mn_lisl,	NULL },
/* 70 */	{ OP_NONE,	"clr",		NULL },
/* 71 */	{ OP_IMM4,	mn_lis,		NULL },
/* 72 */	{ OP_IMM4,	mn_lis,		NULL },
/* 73 */	{ OP_IMM4,	mn_lis,		NULL },
/* 74 */	{ OP_IMM4,	mn_lis,		NULL },
/* 75 */	{ OP_IMM4,	mn_lis,		NULL },
/* 76 */	{ OP_IMM4,	mn_lis,		NULL },
/* 77 */	{ OP_IMM4,	mn_lis,		NULL },
/* 78 */	{ OP_IMM4,	mn_lis,		NULL },
/* 79 */	{ OP_IMM4,	mn_lis,		NULL },
/* 7a */	{ OP_IMM4,	mn_lis,		NULL },
/* 7b */	{ OP_IMM4,	mn_lis,		NULL },
/* 7c */	{ OP_IMM4,	mn_lis,		NULL },
/* 7d */	{ OP_IMM4,	mn_lis,		NULL },
/* 7e */	{ OP_IMM4,	mn_lis,		NULL },
/* 7f */	{ OP_IMM4,	mn_lis,		NULL },
/* 80 */	{ OP_BTREL,	mn_bt,		NULL },
/* 81 */	{ OP_REL,	"bp",		NULL },
/* 82 */	{ OP_REL,	"bc",		NULL },
/* 83 */	{ OP_BTREL,	mn_bt,		NULL },
/* 84 */	{ OP_REL,	"bz",		NULL },
/* 85 */	{ OP_BTREL,	mn_bt,		NULL },
/* 86 */	{ OP_BTREL,	mn_bt,		NULL },
/* 87 */	{ OP_BTREL,	mn_bt,		NULL },
/* 88 */	{ OP_NONE,	"am",		NULL },
/* 89 */	{ OP_NONE,	"amd",		NULL },
/* 8a */	{ OP_NONE,	"nm",		NULL },
/* 8b */	{ OP_NONE,	"om",		NULL },
/* 8c */	{ OP_NONE,	"xm",		NULL },
/* 8d */	{ OP_NONE,	"cm",		NULL },
/* 8e */	{ OP_NONE,	"adc",		NULL },
/* 8f */	{ OP_REL,	"br7",		NULL },
/* 90 */	{ OP_REL,	"br",		NULL },
/* 91 */	{ OP_REL,	"bm",		NULL },
/* 92 */	{ OP_REL,	"bnc",		NULL },
/* 93 */	{ OP_BFREL,	mn_bf,		NULL },
/* 94 */	{ OP_REL,	"bnz",		NULL },
/* 95 */	{ OP_BFREL,	mn_bf,		NULL },
/* 96 */	{ OP_BFREL,	mn_bf,		NULL },
/* 97 */	{ OP_BFREL,	mn_bf,		NULL },
/* 98 */	{ OP_REL,	"bno",		NULL },
/* 99 */	{ OP_BFREL,	mn_bf,		NULL },
/* 9a */	{ OP_BFREL,	mn_bf,		NULL },
/* 9b */	{ OP_BFREL,	mn_bf,		NULL },
/* 9c */	{ OP_BFREL,	mn_bf,		NULL },
/* 9d */	{ OP_BFREL,	mn_bf,		NULL },
/* 9e */	{ OP_BFREL,	mn_bf,		NULL },
/* 9f */	{ OP_BFREL,	mn_bf,		NULL },
/* a0 */	{ OP_FIXED,	mn_ins,		"0" },
/* a1 */	{ OP_FIXED,	mn_ins,		"1" },
/* a2 */	{ OP_ILL,	NULL,		NULL },
/* a3 */	{ OP_ILL,	NULL,		NULL },
/* a4 */	{ OP_IMM4,	mn_ins,		NULL },
/* a5 */	{ OP_IMM4,	mn_ins,		NULL },
/* a6 */	{ OP_IMM4,	mn_ins,		NULL },
/* a7 */	{ OP_IMM4,	mn_ins,		NULL },
/* a8 */	{ OP_IMM4,	mn_ins,		NULL },
/* a9 */	{ OP_IMM4,	mn_ins,		NULL },
/* aa */	{ OP_IMM4,	mn_ins,		NULL },
/* ab */	{ OP_IMM4,	mn_ins,		NULL },
/* ac */	{ OP_IMM4,	mn_ins,		NULL },
/* ad */	{ OP_IMM4,	mn_ins,		NULL },
/* ae */	{ OP_IMM4,	mn_ins,		NULL },
/* af */	{ OP_IMM4,	mn_ins,		NULL },
/* b0 */	{ OP_FIXED,	mn_outs,	"0" },
/* b1 */	{ OP_FIXED,	mn_outs,	"1" },
/* b2 */	{ OP_ILL,	NULL,		NULL },
/* b3 */	{ OP_ILL,	NULL,		NULL },
/* b4 */	{ OP_IMM4,	mn_outs,	NULL },
/* b5 */	{ OP_IMM4,	mn_outs,	NULL },
/* b6 */	{ OP_IMM4,	mn_outs,	NULL },
/* b7 */	{ OP_IMM4,	mn_outs,	NULL },
/* b8 */	{ OP_IMM4,	mn_outs,	NULL },
/* b9 */	{ OP_IMM4,	mn_outs,	NULL },
/* ba */	{ OP_IMM4,	mn_outs,	NULL },
/* bb */	{ OP_IMM4,	mn_outs,	NULL },
/* bc */	{ OP_IMM4,	mn_outs,	NULL },
/* bd */	{ OP_IMM4,	mn_outs,	NULL },
/* be */	{ OP_IMM4,	mn_outs,	NULL },
/* bf */	{ OP_IMM4,	mn_outs,	NULL },
/* c0 */	{ OP_SCR,	mn_as,		NULL },
/* c1 */	{ OP_SCR,	mn_as,		NULL },
/* c2 */	{ OP_SCR,	mn_as,		NULL },
/* c3 */	{ OP_SCR,	mn_as,		NULL },
/* c4 */	{ OP_SCR,	mn_as,		NULL },
/* c5 */	{ OP_SCR,	mn_as,		NULL },
/* c6 */	{ OP_SCR,	mn_as,		NULL },
/* c7 */	{ OP_SCR,	mn_as,		NULL },
/* c8 */	{ OP_SCR,	mn_as,		NULL },
/* c9 */	{ OP_SCR,	mn_as,		NULL },
/* ca */	{ OP_SCR,	mn_as,		NULL },
/* cb */	{ OP_SCR,	mn_as,		NULL },
/* cc */	{ OP_SCR,	mn_as,		NULL },
/* cd */	{ OP_SCR,	mn_as,		NULL },
/* ce */	{ OP_SCR,	mn_as,		NULL },
/* cf */	{ OP_ILL,	NULL,		NULL },
/* d0 */	{ OP_SCR,	mn_asd,		NULL },
/* d1 */	{ OP_SCR,	mn_asd,		NULL },
/* d2 */	{ OP_SCR,	mn_asd,		NULL },
/* d3 */	{ OP_SCR,	mn_asd,		NULL },
/* d4 */	{ OP_SCR,	mn_asd,		NULL },
/* d5 */	{ OP_SCR,	mn_asd,		NULL },
/* d6 */	{ OP_SCR,	mn_asd,		NULL },
/* d7 */	{ OP_SCR,	mn_asd,		NULL },
/* d8 */	{ OP_SCR,	mn_asd,		NULL },
/* d9 */	{ OP_SCR,	mn_asd,		NULL },
/* da */	{ OP_SCR,	mn_asd,		NULL },
/* db */	{ OP_SCR,	mn_asd,		NULL },
/* dc */	{ OP_SCR,	mn_asd,		NULL },
/* dd */	{ OP_SCR,	mn_asd,		NULL },
/* de */	{ OP_SCR,	mn_asd,		NULL },
/* df */	{ OP_ILL,	NULL,		NULL },
/* e0 */	{ OP_SCR,	mn_xs,		NULL },
/* e1 */	{ OP_SCR,	mn_xs,		NULL },
/* e2 */	{ OP_SCR,	mn_xs,		NULL },
/* e3 */	{ OP_SCR,	mn_xs,		NULL },
/* e4 */	{ OP_SCR,	mn_xs,		NULL },
/* e5 */	{ OP_SCR,	mn_xs,		NULL },
/* e6 */	{ OP_SCR,	mn_xs,		NULL },
/* e7 */	{ OP_SCR,	mn_xs,		NULL },
/* e8 */	{ OP_SCR,	mn_xs,		NULL },
/* e9 */	{ OP_SCR,	mn_xs,		NULL },
/* ea */	{ OP_SCR,	mn_xs,		NULL },
/* eb */	{ OP_SCR,	mn_xs,		NULL },
/* ec */	{ OP_SCR,	mn_xs,		NULL },
/* ed */	{ OP_SCR,	mn_xs,		NULL },
/* ee */	{ OP_SCR,	mn_xs,		NULL },
/* ef */	{ OP_ILL,	NULL,		NULL },
/* f0 */	{ OP_SCR,	mn_ns,		NULL },
/* f1 */	{ OP_SCR,	mn_ns,		NULL },
/* f2 */	{ OP_SCR,	mn_ns,		NULL },
/* f3 */	{ OP_SCR,	mn_ns,		NULL },
/* f4 */	{ OP_SCR,	mn_ns,		NULL },
/* f5 */	{ OP_SCR,	mn_ns,		NULL },
/* f6 */	{ OP_SCR,	mn_ns,		NULL },
/* f7 */	{ OP_SCR,	mn_ns,		NULL },
/* f8 */	{ OP_SCR,	mn_ns,		NULL },
/* f9 */	{ OP_SCR,	mn_ns,		NULL },
/* fa */	{ OP_SCR,	mn_ns,		NULL },
/* fb */	{ OP_SCR,	mn_ns,		NULL },
/* fc */	{ OP_SCR,	mn_ns,		NULL },
/* fd */	{ OP_SCR,	mn_ns,		NULL },
/* fe */	{ OP_SCR,	mn_ns,		NULL },
/* ff */	{ OP_ILL,	NULL,		NULL }
};
	
	
/*
 * print error message on stderr
 */
static void
perr(const char *format, ...) {
	va_list params;
	va_start(params, format);
	fprintf(stderr, "%s: ", prog_name);
	vfprintf(stderr, format, params);
	fprintf(stderr, "\n");
	va_end(params);
}


/*
 * print configuration error message on stderr
 */
static void
perr_config(const char *format, ...) {
	va_list params;
	va_start(params, format);
	fprintf(stderr, "%s: %s, line %d: ", prog_name, config_fn, config_ln);
	vfprintf(stderr, format, params);
	fprintf(stderr, "\n");
	va_end(params);
}


/*
 * allocate memory and print an error message if memory is not available
 */
static void *
alloc(size_t s) {
	void *vp = malloc(s);
	if (! s) perr("out of memory");
	return vp;
}


/*
 * parse an address in the config file
 */
static int
parse_address(char *cp, int *addr_p, char **cpp) {
	int rc = 0;
	unsigned long ul;
	if (*cp == '0') {
		if (*(cp + 1) == 'x') {
			/*
			 * hex address
			 */
			ul = strtoul(cp + 2, &cp, 16);
		} else {
			/*
			 * octal address
			 */
			ul = strtoul(cp, &cp, 8);
		}
	} else {
		/*
		 * decimal address
		 */
		ul = strtoul(cp, &cp, 10);
	}
	/*
	 * check syntax and range
	 */
	if ((*cp && ! isspace(*cp)) || ul > 0xffff) {
		perr_config("invalid address");
		rc = (-1);
		goto premature_exit;
	}
	*addr_p = (int) ul;
	*cpp = cp;
premature_exit:
	return rc;
}


/*
 * add a new section to the list of program sections
 */
static int
add_section(int addr, enum section section) {
	int rc = 0;
	struct control **control_pp, *tp;
	/*
	 * skip lower addresses
	 */
	for (control_pp = &control_p; (*control_pp && (*control_pp)->addr
	    < addr); control_pp = &(*control_pp)->next_p);
	/*
	 * addresses must be unique
	 */
	if (*control_pp && (*control_pp)->addr == addr) {
		perr_config("address " FMT_WORD " already defined as %s",
		    (unsigned) addr, section_names[(*control_pp)->section]);
		rc = (-1);
		goto premature_exit;
	}
	/*
	 * allocate and initialize new section
	 */
	tp = alloc(sizeof (struct control));
	if (! tp) {
		rc = (-1);
		goto premature_exit;
	}
	tp->addr = addr;
	tp->section = section;
	tp->next_p = *control_pp;
	*control_pp = tp;
premature_exit:
	return rc;
}


/*
 * read and parse the optional configuration file
 */
static int
read_config(const char *fn, FILE *fp) {
	int rc = 0, addr, i, org_seen = 0;
	char line[80 + 2], *ks_p, *ke_p;
	struct label **label_pp, *tp;
	char *cp, *key = NULL;
	config_fn = fn;
	/*
	 * read lines one by one
	 */
	while ((cp = fgets(line, sizeof line, fp))) {
		config_ln++;
		/*
		 * check for line overflow
		 */
		cp = line + strlen(line) - 1;
		if (*cp != '\n') {
			perr_config("line too long");
			rc = (-1);
			goto premature_exit;
		}
		*cp = '\0';
		/*
		 * skip leading whitespace
		 */
		for (cp = line; isspace(*cp); cp++);
		/*
		 * skip empty lines and comment lines
		 */
		if (! *cp || *cp == '#' || *cp == ';' || *cp == '*') {
			continue;
		}
		/*
		 * isolate first word on line
		 */
		if (! isalpha(*cp)) {
			perr_config("keyword or label doesn\'t start with "
			    "a letter");
			rc = (-1);
			continue;		
		}
		ks_p = cp;
		while (isalnum(*cp) || *cp == '_') cp++;
		ke_p = cp;
		while (isspace(*cp)) cp++;
		free(key);
		key =  alloc(ke_p - ks_p + 1);
		if (! key) {
			rc = (-1);
			goto premature_exit;
		}
		memcpy(key, ks_p, ke_p - ks_p);
		key[ke_p - ks_p] = '\0';
		if (*cp == '=') {
			/*
			 * label definition: skip whitespace after '='
			 */
			for (cp++; isspace(*cp); cp++);
			if (parse_address(cp, &addr, &cp)) {
				rc = (-1);
				continue;
			}
			/*
			 * check if label is already defined
			 */
			for (tp = label_p; tp && strcmp(key, tp->text);
			    tp = tp->next_p);
			if (tp) {
				perr_config("label %s already defined", key);
				rc = (-1);
				continue;
			}
			/*
			 * search for label address
			 */
			for (label_pp = &label_p; *label_pp &&
			    (*label_pp)->addr < addr; label_pp =
			    &(*label_pp)->next_p); 
			/*
			 * error if address has already been named
			 */
			if (*label_pp && (*label_pp)->addr == addr) {
				perr_config("a label is already defined for "
				    "address " FMT_WORD , (unsigned) addr);
				rc = (-1);
				continue;
			}
			/*
			 * allocate new label entry
			 */
			tp = alloc(sizeof (struct label));
			if (! tp) {
				rc = (-1);
				goto premature_exit;
			}
			tp->addr = addr;
			tp->text = key;
			key = NULL;
			tp->next_p = *label_pp;
			*label_pp = tp;
		} else if (! strcmp(key, "org")) {
			/*
			 * set origin: the org directive may be present
			 * only once
			 */
			if (org_seen) {
				perr_config("\"org\" may be specified only "
				    "once");
				rc = (-1);
				continue;
			}
			org_seen = 1;
			if (parse_address(cp, &origin, &cp)) {
				rc = (-1);
				continue;
			}
		} else {
			/*
			 * program section definition?
			 */
			for (i = 0; section_names[i] && strcmp(key,
			    section_names[i]); i++);
			if (section_names[i]) {
				if (parse_address(cp, &addr, &cp)) {
					rc = (-1);
					continue;
				}
				if (add_section(addr, (enum section) i)) {
					rc = (-1);
					continue;
				}
			} else {
				perr_config("invalid keyword \"%s\"", key);
				rc = (-1);
				continue;
			}
		}
		/*
		 * check for additional stuff at line end
		 */
		while (isspace(*cp)) cp++;
		if (*cp) {
			perr_config("extraneous data on line");
			rc = (-1);
		}
	}
	/*
	 * catch read error on the config file
	 */
	if (ferror(fp)) {
		perr_config("file read error: %s", strerror(errno));
		rc = (-1);
		goto premature_exit;
	}		
premature_exit:
	return rc;
}


/*
 * add address as automatically created label; it is no error if there is
 * already a label for the same address (the newer label will be silently
 * dropped)
 */
static int
add_label(int addr) {
	int rc = 0;
	struct label **label_pp, *tp;
	/*
	 * skip labels for addresses lower than addr
	 */
	for (label_pp = &label_p; *label_pp && (*label_pp)->addr < addr;
	    label_pp = &(*label_pp)->next_p);
	/*
	 * if there is not yet a label for addr, add it
	 */
	if (! *label_pp || (*label_pp)->addr > addr) {
		tp = alloc(sizeof (struct label));
		if (! tp) {
			rc = (-1);
			goto premature_exit;
		}
		tp->text = alloc(6);
		if (! tp->text) {
			rc = (-1);
			goto premature_exit;
		}
		tp->addr = addr;
		sprintf(tp->text, "_%04x", addr);
		tp->next_p = *label_pp;
		*label_pp = tp;
	}
premature_exit:
	return rc;
}


/*
 * helper: calculate target of relative branch
 */
static int
target(int pc, unsigned char rel) {
	int w = rel;
	/*
	 * "sign extend" w
	 */
	if (w & 0x80) w = w | 0xff00;
	w += pc + 1;
	return w & 0xffff;
}


/*
 * helper: assemble word from two bytes
 */
static int
word(const unsigned char bytes[2]) {
	int w;
	w = bytes[0];
	w <<= 8;
	w |= bytes[1];
	return w & 0xffff;
}
	

/*
 * first pass: just identify labels for pass_2()
 */
static int
pass_1(void) {
	int rc = 0, loc = 0, pc, l;
	enum section section = SEC_CODE;
	int section_end = l_image;
	struct control *cp = control_p;
	const struct opcode *op;
	while (loc < l_image) {
		pc = loc + origin;
		/*
		 * new section reached?
		 */
		while (cp && cp->addr <= pc) {
			/*
			 * get type and end of new section
			 */
			section = cp->section;
			if (cp->next_p) {
				section_end = cp->next_p->addr - origin;
				if (section_end > l_image) {
					section_end = l_image;
				}
			} else {
				section_end =  l_image;
			}
			cp = cp->next_p;
		}
		/*
		 * process data according to section type
		 */
		switch (section) {
		case SEC_CODE:
			op = opcodes + image[loc];
			l = op_length[op->op_type];
			if (loc + l > section_end) {
				/*
				 * ignore incomplete instructions
				 */
				loc = section_end;
				break;
			}
			/*
			 * we are only interested in branch and jump
			 * instructions; their targets are added to
			 * the list of labels (if they are not already on
			 * this list)
			 */
			switch (op->op_type) {
			case OP_IMM16:
				rc = add_label(word(image + loc + 1));
				if (rc) goto premature_exit;
				break;
			case OP_REL:
			case OP_BTREL:
			case OP_BFREL:
				rc = add_label(target(pc, image[loc + 1]));
				if (rc) goto premature_exit;
				break;
			default:
				break;
			}
			loc += l;
			break;
		case SEC_LABELS:
			/*
			 * words in a label section are added as label
			 * addresses
			 */
			if (loc + 2 > section_end) {
				/*
				 * ignore incomplete word
				 */
				loc = section_end;
				break;
			}
			/*
			 * get big-endian address and add it to labels
			 */
			rc = add_label(word(image + loc));
			if (rc) goto premature_exit;
			loc += 2;
			break;
		default:
			loc++;
			break;
		}
	}
premature_exit:
	return rc;
}


/*
 * parts of the output line; must be set prior to calling print_output().
 * o_label and o_params are  optional (may be NULL), but o_mnemo must be set.
 * o_addr and o_code are only referenced if o_l_code is not zero.
 *
 * param_buffer may be used to assemble instruction parameters.
 * o_column is used to keep track of the tabulation.
 */
static const char *o_label = NULL, *o_mnemo = NULL, *o_params = NULL;
static char param_buffer[1024];
static unsigned char *o_code = NULL;
static int o_addr = 0, o_l_code = 0;
static int o_column = 0;


/*
 * helper functions to format hexadecimal constant parameters
 */
static void
param_byte(unsigned b) {
	sprintf(param_buffer, FMT_BYTE, b);
}

static void
param_word(unsigned w) {
	sprintf(param_buffer, FMT_WORD, w);
}


/*
 * pad output to given tabstop (zero based: 8, 16, 24,...) with tabs; output
 * at least one whitespace character
 */
static void
to_tabstop(int tabstop) {
	if (o_column >= tabstop) {
		printf(" ");
		o_column++;
	} else {
		while (o_column < tabstop) {
			printf("\t");
			o_column = (o_column & (~ 0x7)) + 8;
		}
	}
}


/*
 * format and print an output line using the data above
 */
static void
print_output(void) {
	int i;
	o_column = 0;
	/*
	 * handle label
	 */
	if (o_label) {
		printf("%s", o_label);
		o_column += strlen(o_label);
	}
	/*
	 * handle mnemonic
	 */
	to_tabstop(16);
	printf("%s", o_mnemo);
	o_column += strlen(o_mnemo);
	/*
	 * handle parameters
	 */
	if (o_params) {
		to_tabstop(24);
		printf("%s", o_params);
		o_column += strlen(o_params);
	}
	/*
	 * handle comment (the raw byte values)
	 */
	if (o_l_code) {
		to_tabstop(56);
		printf("; %04x ", o_addr);
		o_column += 7;
		for (i = 0; i < o_l_code; i++) {
			printf("%02x", (unsigned) o_code[i]);
			o_column += 2;
		}
	}
	/*
	 * terminate line
	 */
	printf("\n");
}
	

/*
 * returns the label for an address; the label is certain to exist
 * (otherwise, there is a logical error in the disassembler)
 */
static const char *
find_label(int addr) {
	struct label *lp;
	for (lp = label_p; lp && lp->addr < addr; lp = lp->next_p);
	if (! lp || lp->addr != addr) assert(0);
	return (lp->text);
}


/*
 * textual representations of the legal scratchpad access codes 0..14
 */
static const char *
scr_code[] = {
	"0", "1", "2", "3",
	"4", "5", "6", "7",
	"8", "9", "10", "11",
	"s", "i", "d"
};


/*
 * display l_code (1..6) bytes as hex bytes
 */
static void
handle_bytes(unsigned char *code, int l_code) {
	int i;
	char *cp = param_buffer;
	o_mnemo = mn_db;
	for (i = 0; i < l_code; i++) {
		cp += sprintf(cp, "%s" FMT_BYTE, i ? "," : "",
		    (unsigned) code[i]);
	}
	o_params = param_buffer;
	o_code = code;
	o_l_code = l_code;
	print_output();
}


/*
 * printable ASCII character representations (necessary if the host
 * system of the disassember is not a ASCII system)
 */
static const char ascii[95] =
	" ! #$%& ()*+,-./0123456789:;<=>?"
	"@ABCDEFGHIJKLMNOPQRSTUVWXYZ[ ]^_"
	"`abcdefghijklmnopqrstuvwxyz{|}~";


/*
 * emit l_code (1..6) bytes as ASCII constants
 */
static void
handle_ascii(unsigned char *code, int l_code) {
	int i, c, in_string = 0;
	char *cp = param_buffer;
	o_mnemo = mn_db;
	for (i = 0; i < l_code; i++) {
		c = code[i];
		if (c >= 32 && c <= 126 && c != 34 && c != 39 && c != 92) {
			/*
			 * graphic ASCII characters excluding double quote,
			 * quote, and backslash are collected into literal
			 * strings
			 */
			if (! in_string) {
				if (i) *cp++ = ',';
				*cp++ = '\"';
				in_string = 1;
			}
			*cp++ = ascii[c - 32];
		} else {
			/*
			 * all other bytes are converted to hex bytes
			 */
			if (in_string) {
				*cp++ = '\"';
				in_string = 0;
			}
			if (i) *cp++ = ',';
			cp += sprintf(cp, FMT_BYTE, (unsigned) c);
		}
	}
	if (in_string) *cp++ = '\"';
	*cp = '\0';
	o_params = param_buffer;
	o_code = code;
	o_l_code = l_code;
	print_output();
}


/*
 * emit n_words (1..3) words or labels 
 */
static void
handle_words(unsigned char *code, int n_words, int labels) {
	int i;
	char *cp = param_buffer;
	o_mnemo = "dc.w";
	for (i = 0; i < n_words; i++) {
		if (labels) {
			cp += sprintf(cp, "%s%s", i ? "," : "",
			    find_label(word(code + 2 * i)));
		} else {
			cp += sprintf(cp, "%s" FMT_WORD, i ? "," : "",
			    (unsigned) word(code + 2 * i));
		}
	}
	o_params = param_buffer;
	o_code = code;
	o_l_code = n_words * 2;
	print_output();
}


/*
 * second pass: write disassembled code
 */
static int
pass_2(void) {
	int rc = 0, loc = 0, pc, set_origin = 1, n,
	    section_end = l_image, l;
	enum section section = SEC_CODE;
	struct label *lp = label_p;
	struct control *cp = control_p;
	struct opcode *op;
	/*
	 * emit processor directive
	 */
	o_label = NULL;
	o_mnemo = "processor";
	o_params = "f8";
	o_l_code = 0;
	print_output();
	while (loc < l_image) {
		pc = loc + origin;
		/*
		 * handle section change
		 */
		while (cp && cp->addr <= pc) {
			/*
			 * get type and end of new section
			 */
			section = cp->section;
			if (cp->next_p) {
				section_end = cp->next_p->addr - origin;
				if (section_end > l_image) {
					section_end = l_image;
				}
			} else {
				section_end =  l_image;
			}
			cp = cp->next_p;
		}
		/*
		 * skip unused bytes
		 */
		if (section == SEC_UNUSED) {
			set_origin = 1;
			loc++;
			continue;
		}
		/*
		 * set origin if necessary
		 */
		if (set_origin) {
			/*
			 * emit all labels before the new origin, e.g.
			 * labels pointing to addresses before the image
			 * or labels pointing into unused sections of the
			 * image (i.e., to areas before a set origin directive)
                         */
			o_mnemo = mn_equ;
			o_l_code = 0;
			o_params = param_buffer;
			while (lp && lp->addr < pc) {
				o_label = lp->text;
				param_word(lp->addr);
				print_output();
				lp = lp->next_p;
			}
			/*
			 * emit set origin directive
			 */
			o_label = NULL;
			o_mnemo = "org";
			param_word(pc);
			print_output();
			set_origin = 0;
		}
		/*
		 * set label for the current PC
		 */
		if (lp && lp->addr == pc) {
			o_label = lp->text;
			lp = lp->next_p;
		} else {
			o_label = NULL;
		}
		/*
		 * set raw address and raw code for comment
		 */
		o_addr = pc;
		o_code = image + loc;
		/*
		 * disassemble according to the current program section
		 */
		switch (section) {
		case SEC_CODE:	
			/*
			 * instructions
			 */
			op = opcodes + image[loc];
			/*
			 * if there is only a partial instruction left,
			 * output it as byte definitions
			 */
			l = op_length[op->op_type];
			if (loc + l > section_end) {
				handle_bytes(image + loc, section_end - loc);
				loc = section_end;
				break;
			}
			/*
			 * complete instruction: set raw code length to
			 * instruction length
			 */
			o_mnemo = op->mnemonic;
			o_l_code = l;
			/*
			 * default: there is a parameter, and it is
			 * stored in param_buffer (this will be changed in
			 * some instruction types)
			 */
			o_params = param_buffer;
			switch (op->op_type) {
			case OP_NONE:
				/*
				 * instruction without parameters
				 */
				o_params = NULL;
				break;
			case OP_FIXED:
				/*
				 * instruction with fixed parameters
				 * (e.g., "lr is,a")
				 */
				o_params = op->fixed_param;
				break;
			case OP_IMM8:
				/*
				 * one byte immediate parameter
				 */
				param_byte(image[loc + 1]);
				break;
			case OP_IMM16:
				/*
				 * two byte immediate parameter
				 * ("pi/jmp/dci <addr>")
				 */
				o_params = find_label(word(image + loc + 1));
				break;
			case OP_ILL:
				/*
				 * illegal instruction
				 * (e.g., opcodes 0x2d or 0xff)
				 */
				o_mnemo = mn_db;
				param_byte(image[loc]);
				break;
			case OP_SCR:
				/*
				 * scratchpad reference as only parameter
				 * (e.g., "xs s")
				 */
				o_params = scr_code[image[loc] & 0xf];
				break;
			case OP_ASCR:
				/*
				 * load accumulator from scratchpad
				 * (e.g., "lr a,d");
				 */
				sprintf(param_buffer, "a,%s",
				    scr_code[image[loc] & 0xf]);
				break;
			case OP_SCRA:
				/*
				 * store accumulator to scratchpad
				 * (e.g., "lr d,a");
				 */
				sprintf(param_buffer, "%s,a",
				    scr_code[image[loc] & 0xf]);
				break;
			case OP_REL:
				/*
				 * relative branch without mask
				 * (e.g., "br/br7/bc <addr>")
				 */
				o_params = find_label(target(pc,
				    image[loc + 1]));
				break;
			case OP_BTREL:
				/*
				 * bt instruction
				 */
				sprintf(param_buffer, FMT_DIGIT ",%s",
                                    (unsigned) (image[loc] & 0x7),
				    find_label(target(pc, image[loc + 1])));
				break;
			case OP_BFREL:
				/*
				 * bf instruction
				 */
				sprintf(param_buffer, FMT_DIGIT ",%s",
                                    (unsigned) (image[loc] & 0xf),
				    find_label(target(pc, image[loc + 1])));
				break;
			case OP_IMM3:
				/*
				 * lisu and lisl instructions
				 */
				sprintf(param_buffer, "%d",
				    (int) (image[loc] & 0x7));
				break;
			case OP_IMM4:
				/*
				 * lis, ins, and outs instructions
				 */
				sprintf(param_buffer, "%d",
				    (int) (image[loc] & 0xf));
				break;
			}
			print_output();
			loc += l;
			break;
		case SEC_BYTES:
		case SEC_ASCII:
			/*
			 * bytes and ASCII text are handled essentially
			 * in the same way: up to six bytes are handled
			 * by a single code line, but there may be fewer
			 * if there is a change in section type or a label
			 */
			for (n = 0; n < 8 && loc + n < section_end &&
			    (! lp || pc + n < lp->addr); n++); 
			if (section == SEC_BYTES) {
				handle_bytes(image + loc, n);
			} else {
				handle_ascii(image + loc, n);
			}
			loc += n;
			break;
		case SEC_WORDS:
		case SEC_LABELS:
			/*
			 * words and labels are handled essentially
			 * in the same way: up to three double-byte values
			 * are handled by a single code line, but there may
			 * be fewer if there is a change in section type or
			 * a label
			 */
			for (n = 0; n < 8 && loc + n < section_end &&
			    (! lp || pc + n < lp->addr); n++); 
			if (n / 2) {
				handle_words(image + loc, n / 2,
				    (section == SEC_LABELS));
			}
			/*
			 * handle a single slack byte
			 */
			if (n % 2) {
				if (n / 2) o_label = NULL;
				handle_bytes(image + loc + n - 1, 1);
			}
			loc += n;
			break;
		default:
			/*
			 * SEC_UNUSED already handled above
			 */
			assert(0);
			break;
		}
		/*
		 * update PC and emit all pending labels up to the new PC;
		 * this should only affect labels pointing to the second or
		 * third byte of the preceeding instruction
		 */
		pc = loc + origin;
		o_mnemo = mn_equ;
		o_l_code = 0;
		o_params = param_buffer;
		while (lp && lp->addr < pc) {
			o_label = lp->text;
			/*
			 * these labels are defined relative to the updated PC
			 */
			sprintf(param_buffer, ".-%d", pc - lp->addr);
			print_output();
			lp = lp->next_p;
		}
	}
	/*
	 * end of image: emit all remaining labels
	 */
	o_mnemo = mn_equ;
	o_l_code = 0;
	while (lp) {
		o_label = lp->text;
		param_word(lp->addr);
		o_params = param_buffer;
		print_output();
		lp = lp->next_p;
	}
	/*
	 * emit end statement
	 */
	o_label = NULL;
	o_mnemo = "end";
	o_params = NULL;
	o_l_code = 0;
	print_output();
	return rc;
}


/*
 * main program
 */
int
main(int argc, char **argv) {
	int rc = 0, l;
	FILE *fp = NULL;
	char *fn, *cn, *cp;
	unsigned char *tp;
	/*
	 * isolate base name of program file for error messages
	 */
	for (prog_name = argv[0] + strlen(argv[0]); prog_name != argv[0] &&
	    *(prog_name - 1) != '/'; prog_name--);
	/*
	 * program takes a single positional parameter, the name of the
	 * image file
	 */
	if (argc != 2) {
		perr("usage: %s <filename>", prog_name);
		rc = (-1);
		goto premature_exit;
	}
	fn = argv[1];
	/*
	 * construct name of the configuration file from the name of the
	 * image file: discard an existing extension, add extension ".conf"
	 */
	cp = strrchr(fn, '.');
	l = cp ? (cp - fn) : strlen(fn);
	cn = alloc(l + 6);
	if (! cn) {
		rc = (-1);
		goto premature_exit;
	}
	memcpy(cn, fn, l);
	strcpy(cn + l, ".conf");
	/*
	 * if no configuration file exists, program uses its defaults
	 */
	fp = fopen(cn, "r");
	if (fp) {
		rc = read_config(cn, fp);
		if (rc) goto premature_exit;
		fclose(fp);
	}
	/*
	 * allocate maximal size of image (+1 to catch overlong images)
	 */
	image = alloc(64 * 1024 + 1);
	if (! image) {
		rc = (-1);
		goto premature_exit;
	}
	/*
	 * open image file
	 */
	fp = fopen(fn, "rb");
	if (! fp) {
		perr("cannot open %s: %s", fn, strerror(errno));
		rc = (-1);
		goto premature_exit;
	}
	/*
	 * try to slurp image into main memory
	 */
	l_image = fread(image, 1, 64 * 1024 + 1, fp);
	if (ferror(fp)) {
		perr("%s: %s", fn, strerror(errno));
		rc = (-1);
		goto premature_exit;
	}
	/*
	 * catch overlong images
	 */
	if (! feof(fp) || l_image > 64 * 1024) {
		perr("%s: file too long (max 64 KB)", fn);
		rc = (-1);
		goto premature_exit;
	}
	fclose(fp);
	/*
	 * trim allocated memory to image size
	 */
	tp =  realloc(image, l_image);
	if (! tp) {
		perr("out of memory");
		rc = (-1);
		goto premature_exit;
	}
	image = tp;
	/*
	 * analyze image for jump labels
	 */
	rc = pass_1();
	if (rc) goto premature_exit;
	/*
	 * generate disassembled program
	 */
	rc = pass_2();
	if (rc) goto premature_exit;
	/*
	 * catch write errors
	 */
	if (ferror(stdout)) {
		perr("write error on stdout: %s", strerror(errno));
		rc = (-1);
	}
premature_exit:
	exit(rc ? EXIT_FAILURE : EXIT_SUCCESS);
	return 0;
}
