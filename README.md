# f8utils

Random utilities resulting from my playing around with Fairchild F8/Mostek 3870 hardware and code

## Contents
- `f8dis`: a disassembler for F8/3870 binaries ([man page](https://gitlab.com/gbrein/f8utils/wikis/f8dis.1))

## Author
Georg Brein (`tnylpo@gmx.at`)
