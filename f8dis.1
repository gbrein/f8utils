.\"
.\" Copyright (c) 2022 Georg Brein. All rights reserved.
.\"
.\" Redistribution and use in source and binary forms, with or without
.\" modification, are permitted provided that the following conditions are met:
.\"
.\" 1. Redistributions of source code must retain the above copyright notice,
.\"    this list of conditions and the following disclaimer.
.\"
.\" 2. Redistributions in binary form must reproduce the above copyright
.\"    notice, this list of conditions and the following disclaimer in the
.\"    documentation and/or other materials provided with the distribution.
.\"
.\" 3. Neither the name of the copyright holder nor the names of its
.\"    contributors may be used to endorse or promote products derived from
.\"    this software without specific prior written permission.
.\"
.\" THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
.\" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
.\" IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
.\" ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
.\" LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
.\" CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
.\" SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
.\" INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
.\" CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
.\" ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
.\" POSSIBILITY OF SUCH DAMAGE.
.\"
.TH f8dis 1 2022-07-13
.SH NAME
f8dis \- disassembles Fairchild F8/Mostek 3870 machine code
.SH SYNOPSIS
.HP
.B f8dis
.I <image-file>
.SH DESCRIPTION
f8dis reads a binary image file containing Fairchild F8 machine code
(e.g., a ROM image file) and translates it into a corresponding assembly
language file which it writes to its standard output channel. The details 
of the translation may be controlled by providing a configuration file
which allows the user to specify the load point of the binary image, its code
and data areas, and symbolic addresses for use in the generated assembly
language program.
.PP
The name of the configuration file is derived from the name of the binary
image file name by stripping an optional file extension (i.e., the last dot
in the file name and any characters following it) and by adding the extension
.BR .conf .
If no file of this name is found, f8dis will rely on its minimalistic built-in
defaults.
.SS Configuration file format
.PP
The configuration files used by f8dis are regular text files with a
maximum line length of 80 characters; empty lines and lines starting with
a hash mark
.RB ( # ),
a semicolon 
.RB ( ; ),
or an asterisk
.RB ( * )
are ignored. Every directive or definition has to appear on a separate
line; continuation lines are not supported.
.PP
Addresses may be specified in hexadecimal or octal notation by using
the C language convention of prefixing them with
.B 0x 
respectively 
.BR 0 ;
addresses starting with any other digit are interpreted as decimal.
.PP
The load point of the binary image file may be supplied by the directive
.B org
.IR <address> ,
in case it is different from the default value, 0.
.B org
may be specified at most once per configuration file.
.PP
Program sections in the binary image file may be specified by the
repeated use of the directives
.BR code ,
.BR bytes ,
.BR ascii ,
.BR words ,
and
.BR labels ,
each of which takes a starting address as parameter. These cause the contents
of the binary image file from the given addresses to be interpreted
as processor instructions, data bytes, ASCII text constants, double-byte
values, or address constants, respectively. The additional program section
directive
.B unused
(likewise taking an address parameter) may be used to exclude parts of the
binary image file from the disassembly process.
.PP
Program section directives will affect all
bytes from the given starting address up to the
directive with the next higher address (resp. the end of the image file).
The part of the binary image file prior to the lowest address in any
program section directive will be interpreted as containing processor
instructions.
.PP
Lines of the form
.I <label>
.B =
.I <address>
allow the definition of symbolic names for code and data addresses.
These names will be used in the resulting assembly language file in
preference to address labels generated automatically during the analysis
of the machine code. A
.I <label>
must start with a letter and may contain only letters, digits, and the
underscore character
.RB ( _ ).
Labels must be unique and are case sensitive; their
maximum length is restricted to 78 characters (a side effect of
the line length limit of the configuration file).
.PP
All elements of the configuration file may be used in arbitrary sequence
and need not be ordered by their address values.
.PP
An example configuration file is given below under EXAMPLES.
.SS Output format
The output generated by f8dis is divided into four aligned
columns: labels (starting at position 1), F8 instructions or
assembler directives (17), parameters of the respective instruction or
directive (25),
and a comment column (56) which contains the absolute address and raw data
corresponding to the disassembled program instruction or data definition on
the same line.
All columns but the second may be empty for a particular line. Usually, the
output line length will stay below 80 columns, but output lines may be longer
than this (and their column alignment disturbed), e.g., if the user adds very
long symbol definitions to the configuration file.
.PP
The mnemonics for the F8/3870 instructions and the instruction
parameters conform to the manufacturers' standard, with the notable
exception that they are rendered in
lower-case letters. Assembler directives and the format of numeric
constants and strings are based on the syntax expected by the
.B dasm
assembler, e.g.,
hexadecimal numbers are prefixed by
.B $
(octal constants are never emitted, apart from the obvious exception of
.BR 0 ),
and string constants are surrounded by double quote characters (but
control character escapes like
.B \[rs]r
or
.B \[rs]n
are not used).
.PP
Data definitions use either the directive
.B dc.b
for byte and string data (i.e., for
.B bytes
and
.B ascii
sections) or
.B dc.w
for word and address data
.RB ( words
and
.B labels
sections).
The generated assembly language program is introduced by a
.B processor f8
directive and terminated by
an
.B end
directive.
Label definitions are not terminated by a colon
.RB ( : ),
and symbol definitions take the form of
.I <label>
.B equ
.IR "<value or address>" .
The location counter is represented by a dot
.RB ( . ).
The initial location counter is set and unused areas in the image file
(as specified by
.B unused
section definitions in the configuration file) are skipped by the
.B org
directive.
.PP
Illegal instructions (e.g., the bytes
.B 0x2d
or
.BR 0xff )
and incomplete instructions (e.g., the byte
.B 0x28
as the last byte of a
.B code
section) will appear as
.B dc.b
directives in the generated assembly language program, as will a single
byte at the end of a
.B words
or 
.B labels
section.
.SH OPTIONS
f8dis does not accept any command line options and no positional
command line arguments apart from the name of the binary image file read
as input. 
.SH EXIT STATUS
If f8dis encounters errors in its configuration file or input/output
errors, it will return the exit status 1; a successful execution will cause
it to return 0.
.SH NOTES
During disassembly, f8dis makes two passes over the binary image file:
In the first pass, labels
are automatically generated by collecting the target addresses of
.BR pi ,
.BR jmp ,
and
.B dci
instructions, relative branches, and address constants. During the second
pass, the assembly language output is produced, including both
user-specified labels from the configuration file and labels
autodetected during the first pass.
.PP
While this method works well for static branches, it will fail to
detect target addresses generated dynamically by address arithmetic at
runtime.
.PP
Furthermore, detecting labels automatically will fail spectacularly
if applied to data areas not specified as such in the configuration file.
In fact, the results of applying f8dis to a real-world binary image
file without the benefit of a configuration file providing at least a
rough separation of code and data will be more or less useless.
.PP
Disassembling with f8dis is an iterative process of analyzing the output from
a f8dis run and adapting the configuration file accordingly.
.SH BUGS
Probably some, since f8dis is the product of its author's first exposure
to the Fairchild F8/Mostek 3870 microprocessor architecture.
.SH EXAMPLES
.B f8dis extrom.bin
.PP
will cause f8dis to disassemble the contents of
.B extrom.bin
and write the results to its standard output channel; if the current
directory contains the following file
.BR extrom.conf ,
it will be used to control the details of the disassembly.
.RS
.PP
.nf
# 
# extrom.conf
#
org 0x8000
code 0x8000
labels 0x98aa
words 0x98e0
bytes 0x9e44
ascii 0x9f61
bytes 0x9ff1
# labels
entry = 0x8000
text_pool = 0x9f61
jump_table = 0x98aa
putchar = 0x8122
getchar = 0x821d
.fi
.RE
.PP
.SH AUTHOR
Georg Brein
.RB ( tnylpo@gmx.at )
.SH SEE ALSO
Mostek Corporation,
.I 3870/F8 Microcomputer Data
.IR Book ,
February 1981 (Publication number MK79602).

dasm (https://dasm-assembler.github.io/).
